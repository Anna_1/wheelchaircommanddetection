import struct

from scripts.arduino_external_lib import arduino_receive
from scripts.recorder import Recorder, EMGDataStructure


class EMGDataReader(arduino_receive.SDReader):
    emg_data_buffer: EMGDataStructure or None = None

    def __init__(self):
        self.skip = 100
        self.prev = 0
        self.disconnects = -1
        self.package_lost = 0
        super().__init__()

    def handle_packet(self, packet):
        if len(packet) != 6:
            return
        if self.skip > 0:  # пропускаем первые skip сэмплов
            self.skip -= 1
            return
        s, m, a = struct.unpack('HHH', packet)
        if s != (self.prev + 1) % 65536:
            self.disconnects += 1
            if self.disconnects:
                self.package_lost += (s - self.prev)
        self.prev = s

        emg_ds = EMGDataStructure(seqN=s,
                                  timestamp=m,
                                  signal=a,
                                  disconnects=self.disconnects,
                                  package_lost=self.package_lost)
        self.emg_data_buffer = emg_ds
        # print("handle_packet:", len(self.emg_data_buffer), emg_ds.data)


class EMGDataRead2Record(EMGDataReader):

    def __init__(self):
        self.recorder = None
        super().__init__()

    def set_recorder(self, recorder: Recorder):
        if not self.recorder:
            self.recorder = recorder

    def handle_packet(self, packet):
        super().handle_packet(packet)
        if self.recorder and self.recorder.enabled and self.emg_data_buffer:
            self.recorder.emg_data_buffer = self.emg_data_buffer
            self.recorder.log_data()
            self.emg_data_buffer = None
