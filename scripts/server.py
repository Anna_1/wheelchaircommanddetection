import socket
import time
from typing import Generator

from scripts.command_definer import CommandDefiner, Definer
from scripts.realtime_scan import realtime_feature_generator_wrapper as feature_generator
from scripts.simulation import lidar
from scripts.simulation.controls_resolver import ControlsResolver

_cont_resolver = ControlsResolver()

default_encoding = 'utf-8'
packet_termination_char = "\n"
_packets = list()

_default_port = 9990
_listeners_count = 1


def define_controls(command: str, json_string: str) -> str:
    collisions = lidar.get_collisions(json_string=json_string)
    command = _cont_resolver.resolve_collision(command, collisions)
    controls = _cont_resolver.get_controls_string(command)
    controls += ";" + command

    return controls


def packet_data_to_controls(packet_data: str, command_definer: Definer) -> str:
    _cont_resolver.config_from_json(json_string=packet_data)

    command = command_definer.defined_command
    print(f"Get command: {command}")

    return define_controls(command=command, json_string=packet_data)


def receive_bytes_recursive(packet_data: str, packet_part: str) -> str:
    termination_char_pos = packet_part.find(packet_termination_char)
    if termination_char_pos == -1:
        packet_data += packet_part
        return packet_data

    packet_data += packet_part[:termination_char_pos]
    print("Received %s" % packet_data)

    if packet_data == "0":
        return packet_data

    _packets.append(packet_data)

    packet_data = receive_bytes_recursive("", packet_part[termination_char_pos + 1:])
    return packet_data


def process_recent_packet(connection: socket, command_definer: Definer):
    if len(_packets) > 0:
        recent_msg_index = len(_packets) - 1

        controls = packet_data_to_controls(packet_data=_packets[recent_msg_index], command_definer=command_definer)
        print("Send %s" % controls.encode(default_encoding))
        connection.send(controls.encode(default_encoding))

        _packets.clear()
        time.sleep(0.1)


def init_command_definer(get_random: bool = False,
                         random_delay: int or float = 5.0,
                         infinite_generator: Generator or None = None) -> CommandDefiner:
    definer_ = CommandDefiner(get_random=get_random)

    if get_random:
        definer_.start_command_daemon(delay=random_delay)
    else:
        definer_.start_command_daemon(feature_generator=infinite_generator)

    return definer_


def default_feature_generator(save_plots: bool = False):
    return feature_generator(save_plots=save_plots)


def run_server(port: int = _default_port,
               receive_bytes: int = 1024,
               save_plots: bool = False,
               get_random: bool = False,
               random_delay: int or float = 5.0,
               infinite_generator: Generator or None = None,
               definer: Definer or None = None):
    """ Defines variables and default workflow for realtime_feature_generator function

        port -- port to bind socket
        receive_bytes -- amount of bytes to read from socket
        save_plots -- defines if plots should be saved, not recommended to set it as True (default False)
        get_random -- defines if commands should be random or parsed from realtime data (default False)
        random_delay -- delay for updating random command, if get_random is True
        definer -- supplies commands (default command_definer.CommandDefiner daemons)
        infinite_generator -- generator extracting features from realtime data, if get_random is False
                              (default realtime_scan.realtime_feature_generator)

    """

    if not get_random and not infinite_generator:
        print("No generator specified. Setting default feature generator")
        infinite_generator = default_feature_generator(save_plots=save_plots)

    if not definer:
        definer = init_command_definer(get_random=get_random,
                                       random_delay=random_delay,
                                       infinite_generator=infinite_generator)

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(('', port))
    print("Server is started")

    # listening port
    sock.listen(_listeners_count)
    (conn, addr) = sock.accept()
    print("Accepted a connection request from %s:%s" % (addr[0], addr[1]))

    data = ""
    while True:
        try:
            recent_packet_path = conn.recv(receive_bytes).decode(default_encoding)
        except ConnectionResetError:
            print("An existing connection was forcibly closed by client")
            break

        data = receive_bytes_recursive(packet_data=data, packet_part=recent_packet_path)

        if data == "0":
            break

        process_recent_packet(connection=conn, command_definer=definer)

    print("Disconnected client %s:%s" % (addr[0], addr[1]))
    conn.close()


if __name__ == '__main__':
    run_server(save_plots=False,
               get_random=False,
               random_delay=5)
