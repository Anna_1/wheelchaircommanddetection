import os.path
import serial
from typing import Generator

from scripts import port, baudrate
from scripts import functions
from scripts import running_window
from scripts import EMG_fft as Ef


def read_serial_generator(serial_port: serial.Serial) -> Generator:
    """Read input data x(t) from Serial port"""

    while True:
        emg_signal = functions.read_emg_once(serial_port)
        if emg_signal:
            yield emg_signal


def realtime_feature_generator(command: str, serial_port: serial.Serial,
                               window_size: int, window_step: int, sample_time: float,
                               output_plot_name: str or None, res_file_name: str,
                               file_name: str = "-",
                               text_result: bool = True,
                               csv_result: bool = True) -> Generator:
    """Process data from serial port realtime

        Keyword arguments:
        command -- key to be written in results
        serial_port -- existing Serial port to read data
        window_size -- running window size
        window_step -- running window increment step
        sample_time -- Serial sample time for FFT calculation
        output_plot_name -- path and file name without extension to save plots
        res_file_name -- base name for result files, can be set as "" if both text_result and csv_result are False
        file_name -- key to be written in results (default "-")
        text_result -- defines if results should be appended to .txt file (default True)
        csv_result -- defines if results should be appended to .csv file (default True)

        """

    print(f"sample time = {sample_time}")

    generator = read_serial_generator(serial_port)

    efg = Ef.emg_features_generator(generator=generator,
                                    window_size=window_size,
                                    window_step=window_step,
                                    sample_time=sample_time,
                                    name=output_plot_name,
                                    show=False)

    for wn, x_mean, f_max_dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt, \
            emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl in efg:
        running_window.append_result_line(command, file_name, wn,
                                          x_mean, f_max_dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt,
                                          emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl,
                                          res_file_name, text_result, csv_result)
        yield command, wn, x_mean, f_max_dict, emg_rms, emg_mav, emg_wl


def realtime_feature_generator_wrapper(port_: str = port,
                                       baudrate_: int = baudrate,
                                       use_existing_serial: bool = False,
                                       serial_port: serial.Serial or None = None,
                                       result_path: str = "realtime_scan",
                                       log_name: str = "RealtimeScan",
                                       log_file: str = "SerialPort",
                                       window_size: int = 256,
                                       window_step: int = 128,
                                       sample_time: int = 0.001,
                                       clean_previous_results: bool = True,
                                       text_result: bool = True,
                                       csv_result: bool = True,
                                       save_plots: bool = True):
    """Defines variables and default workflow for realtime_feature_generator function

    Keyword arguments:
    _port -- port value for Serial port (default - default packet port)
    _baudrate -- baudrate value for Serial (default - default packet baudrate)
    use_existing_serial -- defines if existing Serial port should be used (default False)
    serial_port -- existing Serial port to read data, can be set as None if use_existing_serial is False (default None)
    result_path -- directory to save outputs (default "realtime_scan")
    log_name -- subdirectory to save plots, also key to be written in results (default "SerialPort")
    log_file -- key to be written in results (default "RealtimeScan")
    window_size -- running window size (default 256)
    window_step -- running window increment step (default 128)
    sample_time -- Serial sample time for FFT calculation (default 0.001)
    clean_previous_results -- defines if previous results should be removed (default True)
    text_result -- defines if results should be appended to .txt file (default True)
    csv_result -- defines if results should be appended to .csv file (default True)
    save_plots -- defines if plots should be saved (default True)

    """

    if use_existing_serial:
        ser = serial_port
    else:
        ser = serial.Serial(port_, baudrate_)

    output_plot_name = None
    if save_plots:
        output_plot_name = os.path.join(result_path, log_name, "plot")
    res_file = os.path.join(result_path, "res")

    # prepare result directories
    functions.prepare_directory(result_path, clean_previous_results)
    if save_plots:
        functions.prepare_directory(os.path.join(result_path, log_name))

    # prepare result files
    if text_result and csv_result:
        functions.prepare_file(f"{res_file}.txt")
        functions.prepare_file(f"{res_file}.csv")

        with open(f"{res_file}.csv", 'a') as rf_csv:
            rf_csv.write("name;fileN;frame;mean;RMS;MAV;WL;ZC;SKEW;KURT;"
                         "IAV;SSC;DAMV;DASDV;MPV;MNF;MDF;FD_WL;f_max1;amp1;f_max2;amp2;f_max3;amp3\n")
    else:
        res_file = ""

    # get realtime emg feature generator
    generator = realtime_feature_generator(command=log_name,
                                           serial_port=ser,
                                           window_size=window_size,
                                           window_step=window_step,
                                           sample_time=sample_time,
                                           output_plot_name=output_plot_name,
                                           res_file_name=res_file,
                                           file_name=log_file,
                                           text_result=text_result,
                                           csv_result=csv_result)

    return generator


if __name__ == '__main__':
    g = realtime_feature_generator_wrapper()

    for pdr in g:
        pass
