import json


def get_collisions(json_string: str) -> list:
    json_data = parse_json(json_string)

    collisions = list()
    if "frontLidar" in json_data:
        collisions.append("GO")
    if "backLidar" in json_data:
        collisions.append("BACK")

    return collisions


def parse_json(json_string: str):
    json_data = json.loads(json_string)
    return json_data


if __name__ == '__main__':
    test_str = '{"frontLidar":[{"d":0.99,"rx":10,"ry":4},{"d":0.99,"rx":0,"ry":8},{"d":0.98,"rx":10,"ry":8}],"backLidar":[{"d":0.79,"rx":0,"ry":4},{"d":0.78,"rx":10,"ry":4},{"d":0.78,"rx":0,"ry":8},{"d":0.77,"rx":10,"ry":8}]}'

    result = get_collisions(json_string=test_str)
    print(result)
