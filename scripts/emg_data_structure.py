class EMGDataStructure:
    _data = dict()

    def __init__(self, seqN, timestamp, signal, disconnects, package_lost):
        self.seqN = seqN
        self.timestamp = timestamp
        self.signal = signal
        self.disconnects = disconnects
        self.package_lost = package_lost

    @property
    def data(self):
        if len(self._data) == 0:
            self._data = {
                "seqN": self.seqN,
                "timestamp": self.timestamp,
                "signal": self.signal,
                "disconnects": self.disconnects,
                "pkg_lost": self.package_lost
            }
        return self._data
