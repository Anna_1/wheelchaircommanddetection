import os.path

port = 'COM8'
baudrate = 115200

name_type = 1  # 1 - наименования команд и иерархия файлов Анны, 2 - Александры.

if name_type == 1:
    out_path = 'output'
    result_path = os.path.join(out_path, "res")
    command_dict = {
        "WHEELCHAIR": "кресло",
        "STOP": "стоп",
        "GO": "вперёд",
        "BACK": "назад",
        "RIGHT": "вправо",
        "LEFT": "влево",
        # "SIMPLE": "нет_команд"
    }
elif name_type == 2:
    out_path = 'output2'
    result_path = os.path.join(out_path, "res")
    command_dict = {
        "Указательный": "Указательный",
        "Средний": "Средний",
        "Мизинец": "Мизинец",
        "Большой": "Большой",
        "Безымянный": "Безымянный",
        "Без напряжения": "Без напряжения"
    }

recorder_event_codes = {
    "WHEELCHAIR": 10,
    "WHEELCHAIR_UP": 11,
    "STOP": 20,
    "STOP_UP": 21,
    "GO": 30,
    "GO_UP": 31,
    "BACK": 40,
    "BACK_UP": 41,
    "RIGHT": 50,
    "RIGHT_UP": 51,
    "LEFT": 60,
    "LEFT_UP": 61,
    "SIMPLE": 0
}

SMOOTH_SPAN = 30
TREND_SPAN = 300
NOTCH_FREQ = 50.0
NOTCH_BANDWIDTH = 1.0
