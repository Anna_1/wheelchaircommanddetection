import os.path
from statistics import mean

from scripts import out_path, command_dict, result_path
from scripts import functions
import scripts.EMG_fft as Ef


if __name__ == '__main__':
    sample_time = 0.001

    for key, val in command_dict.items():
        print(f"Следующая команда: {val}")

        input_command_path = os.path.join(out_path, key)
        result_command_path = os.path.join(result_path, key)
        functions.prepare_directory(result_command_path)

        for filename in os.listdir(input_command_path):
            input_file = os.path.join(input_command_path, filename)
            short_name = str(filename.split('.')[0])
            output_plot_name = os.path.join(result_command_path, short_name)

            data = Ef.read_input_data_to_float(input_file)

            print(input_file)
            print(f"sample time = {sample_time}")

            x_mean = mean(data)  # находим среднее значение сигнала
            x = [z - x_mean for z in data]  # вычитаем среднне значение из исходного сигнала. x(t) = x1-x_mean

            Ef.emg_fft(x_input=x, sample_time=sample_time, name=output_plot_name, show=False)

            print("==============")
