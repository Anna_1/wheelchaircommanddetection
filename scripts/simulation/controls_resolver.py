import json


class Controls:
    left_wheel_motor_torque = 0.0
    right_wheel_motor_torque = 0.0
    left_wheel_brake_torque = 0.0
    right_wheel_brake_torque = 0.0
    steering_left = -1.0
    steering_right = -1.0

    def __str__(self) -> str:
        return "%0.2f;%0.2f;%0.2f;%0.2f;%0.2f;%0.2f" % \
               (self.left_wheel_motor_torque,
                self.right_wheel_motor_torque,
                self.left_wheel_brake_torque,
                self.right_wheel_brake_torque,
                self.steering_left,
                self.steering_right)


class ControlsResolver:
    _max_motor_torque: float or int  # maximum torque the motor can apply to wheel
    _max_brake_torque: float or int  # maximum brake torque can be applied to wheel
    _steering_motor_torque: float or int  # torque the motor applies to wheel while steering

    _base_adjacent_steering_angle: float or int  # передавать снаружи
    _base_opposite_steering_angle: float or int  # передавать снаружи

    _last_command = ""

    _forward_direction = True

    def __init__(self):
        self.config()

    def config(self,
               max_motor_torque: float or int = 4.0,
               max_brake_torque: float or int = 10.0,
               steering_motor_torque: float or int = 2.0,
               base_adjacent_steering_angle: float or int = 90.0,
               base_opposite_steering_angle: float or int = 30.0
               ):
        self._max_motor_torque = max_motor_torque
        self._max_brake_torque = max_brake_torque
        self._steering_motor_torque = steering_motor_torque

        self._base_adjacent_steering_angle = base_adjacent_steering_angle
        self._base_opposite_steering_angle = base_opposite_steering_angle

    def config_from_json(self, json_string: str):
        json_data = json.loads(json_string)
        if "maxMotorTorque" in json_data:
            self._max_motor_torque = float(json_data["maxMotorTorque"])
        if "maxBrakeTorque" in json_data:
            self._max_brake_torque = float(json_data["maxBrakeTorque"])
        if "steeringMotorTorque" in json_data:
            self._steering_motor_torque = float(json_data["steeringMotorTorque"])
        if "baseAdjacentSteeringAngle" in json_data:
            self._base_adjacent_steering_angle = float(json_data["baseAdjacentSteeringAngle"])
        if "baseOppositeSteeringAngle" in json_data:
            self._base_opposite_steering_angle = float(json_data["baseOppositeSteeringAngle"])

    def whturn(self, forward: bool, right: bool) -> str:
        steering_motor = self._steering_motor_torque
        if not self._forward_direction:
            steering_motor *= -1  # if WH moves back motor direction changes to opposite

        controls = Controls()

        controls.left_wheel_motor_torque = steering_motor
        controls.right_wheel_motor_torque = steering_motor

        if forward and right:
            controls.left_wheel_brake_torque = 0
            controls.right_wheel_brake_torque = self._max_brake_torque

            controls.steering_left = self._base_opposite_steering_angle
            controls.steering_right = self._base_adjacent_steering_angle

        if forward and not right:
            controls.left_wheel_brake_torque = self._max_brake_torque
            controls.right_wheel_brake_torque = 0

            controls.steering_left = 360.0 - self._base_adjacent_steering_angle
            controls.steering_right = 360.0 - self._base_opposite_steering_angle

        if not forward and right:
            controls.left_wheel_brake_torque = self._max_brake_torque
            controls.right_wheel_brake_torque = 0

            controls.steering_left = 180.0 - self._base_adjacent_steering_angle
            controls.steering_right = 180.0 - self._base_opposite_steering_angle

        if not forward and not right:
            controls.left_wheel_brake_torque = 0
            controls.right_wheel_brake_torque = self._max_brake_torque

            controls.steering_left = 180.0 + self._base_opposite_steering_angle
            controls.steering_right = 180.0 + self._base_adjacent_steering_angle

        return str(controls)

    def whmove(self, forward: bool) -> str:
        motor = self._max_motor_torque

        controls = Controls()

        if forward:
            self._forward_direction = True
            controls.steering_left = 0.0
            controls.steering_right = 0.0
        else:
            motor *= -1  # if WH moves back motor direction changes to opposite
            self._forward_direction = False
            controls.steering_left = 180.0
            controls.steering_right = 180.0

        controls.left_wheel_brake_torque = 0.0
        controls.right_wheel_brake_torque = 0.0
        controls.left_wheel_motor_torque = motor
        controls.right_wheel_motor_torque = motor

        return str(controls)

    def whstop(self):
        controls = Controls()
        controls.left_wheel_brake_torque = self._max_brake_torque
        controls.right_wheel_brake_torque = self._max_brake_torque

        return str(controls)

    def resolve_collision(self, command: str, collisions: list) -> str:
        if "GO" in collisions and ((self._last_command == "GO" and command == "update") or command == "GO"):
            command = "STOP"
        if "BACK" in collisions and ((self._last_command == "BACK" and command == "update") or command == "BACK"):
            command = "STOP"

        if command == "update":
            command = self._last_command

        return command

    def get_controls_string(self, command: str) -> str:
        # print("got command "+command)
        self._last_command = command

        if command == "STOP":
            return self.whstop()
        elif command == "GO":
            return self.whmove(forward=True)
        elif command == "BACK":
            return self.whmove(forward=False)
        elif command == "LEFT":
            return self.whturn(forward=self._forward_direction, right=False)
        elif command == "RIGHT":
            return self.whturn(forward=self._forward_direction, right=True)
        else:
            raise Exception(f"Unknown command: {command}")


if __name__ == '__main__':
    act = ControlsResolver()
    print("GO: " + act.get_controls_string("GO"))
    print("STOP: " + act.get_controls_string("STOP"))
    print("LEFT_GO: " + act.get_controls_string("LEFT"))
    print("RIGHT_GO: " + act.get_controls_string("RIGHT"))
    print("BACK: " + act.get_controls_string("BACK"))
    print("LEFT_BACK: " + act.get_controls_string("LEFT"))
    print("RIGHT_BACK: " + act.get_controls_string("RIGHT"))
    print("STOP: " + act.get_controls_string("STOP"))
    # print(act.get_controls_string("STOPGO"))

    # tt = '{"baseAdjacentSteeringAngle":82.43343,"baseOppositeSteeringAngle":39.64263}'
    # act.config_from_json(tt)
    # print(act._base_adjacent_steering_angle)
    # print(act._base_opposite_steering_angle)
    # print(act._max_brake_torque)
    # print(act._max_motor_torque)
    # print(act._steering_motor_torque)
