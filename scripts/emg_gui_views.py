from PyQt5.QtWidgets import QFrame, QPushButton, QHBoxLayout, QVBoxLayout, QLabel


class EmgGUIView(QFrame):
    def __init__(self, parent):
        super().__init__(parent)

        self.initFrame()

    def initFrame(self):
        pass

    def show_view(self):
        pass

    def clear_layout(self, layout):
        if layout is not None:
            while layout.count():
                child = layout.takeAt(0)
                if child.widget() is not None:
                    child.widget().setParent(None)
                elif child.layout() is not None:
                    self.clear_layout(child.layout())


class EmgStartView(EmgGUIView):
    start_button_style = '{background-color: green; color: white; font-size: 40px;}'

    def initFrame(self):
        self.start_button = None

    def show_view(self):
        btn = QPushButton('НАЧАТЬ')
        btn.setStyleSheet(f"QPushButton {self.start_button_style}")
        self.start_button = btn

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(btn)
        hbox.addStretch(1)

        vbox: QVBoxLayout = self.parent().layout()
        vbox.addLayout(hbox)


class EmgMainView(EmgGUIView):
    stop_button_style = '{background-color: red; color: white; font-size: 20px;}'
    command_label_style = '{color: red; font-size: 40px;}'

    def initFrame(self):
        self.command_label = None
        self.count_label = None
        self.exit_button = None

    def show_view(self):
        btn = QPushButton('остановить')
        btn.setStyleSheet(f"QPushButton {self.stop_button_style}")
        self.exit_button = btn

        lbl = QLabel("")
        lbl.setStyleSheet(f"QLabel {self.command_label_style}")

        lbl2 = QLabel("")

        hlblbox = QHBoxLayout()
        hlblbox.addStretch(1)
        hlblbox.addWidget(lbl)
        hlblbox.addStretch(1)

        hbtnbox = QHBoxLayout()
        hbtnbox.addWidget(lbl2)
        hbtnbox.addStretch(1)
        hbtnbox.addWidget(btn)

        vbox: QVBoxLayout = self.parent().layout()
        vbox.addStretch(1)
        vbox.addLayout(hlblbox)
        vbox.addStretch(1)
        vbox.addLayout(hbtnbox)

        self.command_label = lbl
        self.count_label = lbl2
