import os.path
import re
import serial
import shutil


def prepare_directory(directory: str, clean: bool = False) -> None:
    if not os.path.isdir(directory):
        os.makedirs(directory)
    elif clean:
        shutil.rmtree(directory)
        os.makedirs(directory)


def prepare_file(file: str) -> None:
    fl = open(file, 'w', encoding='utf-8')
    # fl.write(str(datetime.datetime.now()) + '\n')
    fl.close()


def read_emg_once(serial_port: serial.Serial) -> str or None:
    # Read one line
    line = serial_port.readline()

    # Parse the line to extract the signal value
    signal_search = re.search('([0-9]+)', line.decode('utf-8'))
    if signal_search:
        signal = signal_search.group(1)
        print(signal)
        return signal
    return None
