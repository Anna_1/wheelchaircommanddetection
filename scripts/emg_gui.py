import sys
from threading import Lock

from PyQt5.QtWidgets import QApplication, QMainWindow, QDesktopWidget, \
    QVBoxLayout, QFrame
from PyQt5.QtCore import QBasicTimer

from scripts import command_dict
from scripts.emg_gui_views import EmgGUIView, EmgStartView, EmgMainView
from scripts.recorder import Recorder, EMGSimpleRecorder
from scripts.recorder_manager import RecorderManager, EMGRecorderProtocolManager

_lock = Lock()

command_dict_reversed = {val: key for key, val in command_dict.items()}


class EmgGUI(QMainWindow):
    WINDOW_WIDTH = 700
    WINDOW_HEIGHT = 500

    DEFAULT_REC_MAN_FACTORY = EMGRecorderProtocolManager
    DEFAULT_REC_FACTORY = EMGSimpleRecorder

    def __init__(self, iter_count: int = 10, sep_character: str = ';',
                 rec_manager_factory: type = DEFAULT_REC_MAN_FACTORY,
                 rec_factory: type = DEFAULT_REC_FACTORY,
                 rec_m_ch="EVENT_MARKER", *rec_init_args, **rec_init_kwargs):
        super().__init__()

        self.iter_count = iter_count

        self.sep_character = sep_character
        self.m_ch = rec_m_ch
        self.rec_init_args = rec_init_args
        self.rec_init_kwargs = rec_init_kwargs

        self._rec_manager_factory = rec_manager_factory
        self._recorder_factory = rec_factory

        self.emg_frame = None
        self.initUI()

    def initUI(self):
        self.emg_frame = EmgFrame(self)

        rec = self.init_recorder(sep_character=self.sep_character,
                                 rec_factory=self._recorder_factory,
                                 marker_channel=self.m_ch,
                                 *self.rec_init_args,
                                 **self.rec_init_kwargs)

        self.emg_frame.set_recorder(rec, self._rec_manager_factory)
        self.setCentralWidget(self.emg_frame)

        self.emg_frame.start(self.iter_count)

        self.resize(self.WINDOW_WIDTH, self.WINDOW_HEIGHT)
        self.center()
        self.setWindowTitle('EMG-Data Acquisition')
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    @staticmethod
    def init_recorder(sep_character: str = ';',
                      rec_factory: type = DEFAULT_REC_FACTORY,
                      marker_channel=None,
                      *rec_init_args,
                      **rec_init_kwargs):

        rec = rec_factory(marker_channel, *rec_init_args, **rec_init_kwargs)
        rec.sep_character = sep_character

        return rec


class EmgFrame(QFrame):
    DEFAULT_RECORD_TIME = 2000

    is_active: bool
    prev_command: str
    view: EmgGUIView or None

    _rec_manager: RecorderManager or None

    def __init__(self, parent):
        super().__init__(parent)

        self.initFrame()

    def initFrame(self):
        self._rec_manager = None

        self.com_gen = None

        self.view = None
        self.command_label = None
        self.setLayout(QVBoxLayout())

        self.timer = QBasicTimer()

    # Recorder section #

    def set_recorder(self, recorder: Recorder, rec_manager_factory: type = RecorderManager):
        print("set_recorder")
        self._rec_manager = rec_manager_factory()
        self._rec_manager.set_recorder(recorder=recorder)

    # GUI section #

    def show_start_screen(self):
        self.view = EmgStartView(self)
        self.view.show_view()
        self.view.start_button.clicked.connect(self.start_button_clicked)

    def show_main_screen(self):
        self.view = EmgMainView(self)
        self.view.show_view()
        self.view.exit_button.clicked.connect(sys.exit)
        self.command_label = self.view.command_label

    def update_command(self, command):
        if not self.command_label:
            return

        self.command_label.setText(str(command))

    # Logic section #

    def start(self, iter_count: int = 10):
        self.com_gen = self.sequence_command_generator(iter_count)
        self.show_start_screen()

    def start_button_clicked(self):
        self.is_active = True

        self.view.clear_layout(self.layout())
        self.show_main_screen()

        self._rec_manager.start_recorder()
        self.timer.start(1, self)

    def timerEvent(self, event):
        """ Called every DEFAULT_RECORD_TIME milliseconds until the end of the command sequence """

        command = self.next_command()

        with _lock:
            self.update_command(command)

        command = "SIMPLE" if command == "" else command_dict_reversed[command]

        self._rec_manager.update_recorder_event(command=command)

        if self.is_active:
            self.timer.start(self.DEFAULT_RECORD_TIME, self)

    # Other #

    # @staticmethod
    def sequence_command_generator(self, iter_count: int = 10):
        for i in range(iter_count, 0, -1):
            self.view.count_label.setText(str(i-1))
            for command in command_dict_reversed.keys():
                yield ""
                yield command

    def next_command(self):
        try:
            return next(self.com_gen)
        except StopIteration:
            self.is_active = False
            return ""


def run(*args, **kwargs):
    app = QApplication(sys.argv)
    ex = EmgGUI(*args, **kwargs)
    sys.exit(app.exec_())


if __name__ == '__main__':
    run(iter_count=2,
        profiling_mode=True)
