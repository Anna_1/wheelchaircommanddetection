import argparse

from scripts import server

_default_port = 9990

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='server script')
    parser.add_argument('--port', type=int, default=_default_port, help='port to bind socket')
    parser.add_argument('--receive_bytes', type=int, default=1024, help='number of bytes to read from socket')
    parser.add_argument('--save_plots', action='store_true', help='defines if plots should be saved')
    parser.add_argument('--get_random', action='store_true', help='defines if commands should be random or not')
    parser.add_argument('--random_delay', type=int or float, default=5.0, help='delay for updating random command')
    args = parser.parse_args()

    print(args)

    server.run_server(port=args.port,
                      receive_bytes=args.receive_bytes,
                      save_plots=args.save_plots,
                      get_random=args.get_random,
                      random_delay=args.random_delay)

# python run_server.py --get_random
