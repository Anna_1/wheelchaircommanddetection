from abc import ABC, abstractmethod
from distutils.log import warn
from time import time_ns
import serial

from scripts import port, baudrate
from scripts import functions
from scripts.emg_data_structure import EMGDataStructure


class Recorder(ABC):
    default_logfile = "log.csv"
    sep_character = ","
    enabled = True

    def __init__(self, channels=None, marker_channel=None) -> None:
        if not channels:
            channels = []

        self.logname = self.default_logfile
        self.log = None

        self.marker_channel = marker_channel
        self.channels = channels

        self.current_event = 0
        self.event_cnt = 0

    def set_event(self, val=10):
        self.current_event = val
        self.event_cnt += 1

    def clear_event(self, last_cnt):
        if last_cnt == self.event_cnt:
            self.current_event = 0

    def start_recording(self):
        self.enabled = True
        self.prepare_log_file()
        self.event_cnt = 0
        self.log_data()

    @abstractmethod
    def log_data(self):
        pass

    def prepare_log_file(self):
        header = self.sep_character.join(self.channels) + "\n"
        self.log = open(self.logname, "w")
        self.log.write(header)

    def set_log_id(self, id):
        self.logname = f"log_{id}.csv"


class EMGRecorder(Recorder):

    def __init__(self, marker_channel=None, profiling_mode=False) -> None:
        self.ser = serial.Serial(port, baudrate)
        self.channels = ["A1"]
        self.profiling_mode = profiling_mode
        if marker_channel:
            self.channels.append(marker_channel)
            self.channels.append("CNT")

        if self.profiling_mode:
            self.channels.append("TIMESTAMP")
            self.channels.append("ELAPSED")
        super().__init__(channels=self.channels, marker_channel=marker_channel)

    def log_data(self):
        if not self.ser:
            return

        last_loop_begin = time_ns()
        while True:
            if not self.enabled:
                break
            read_begin = time_ns()
            loop_elapsed = read_begin - last_loop_begin
            last_loop_begin = read_begin

            try:
                emg_signal = functions.read_emg_once(self.ser)
            except Exception as e:
                print(e)
                continue

            read_end = time_ns()
            if not emg_signal:
                warn("Data is not fetched this time")
                continue

            current_event_cnt = self.event_cnt
            data = [str(emg_signal)]
            if self.marker_channel:
                data.append(str(self.current_event))
                data.append(str(current_event_cnt))
                self.clear_event(current_event_cnt)

            if self.profiling_mode:
                # data.append(str(read_end))
                data.append(str(read_end - read_begin))
                data.append(str(loop_elapsed))

            self.log.write(self.sep_character.join(data) + "\n")


class EMGSimpleRecorder(Recorder):

    def __init__(self, marker_channel=None, profiling_mode=False) -> None:
        self.channels = ["A1"]
        self.profiling_mode = profiling_mode

        if marker_channel:
            self.channels.append(marker_channel)
            self.channels.append("CNT")

        if self.profiling_mode:
            self.channels.append("TIMESTAMP")
            self.channels.append("SEQ_N")

        super().__init__(channels=self.channels, marker_channel=marker_channel)

        self.emg_data_buffer: EMGDataStructure or None = None

    def log_data(self):
        if not self.emg_data_buffer:
            warn("Data is not fetched this time")
            return

        # print(self.emg_data_buffer)
        current_event_cnt = self.event_cnt
        data = [str(self.emg_data_buffer.signal)]
        if self.marker_channel:
            data.append(str(self.current_event))
            data.append(str(current_event_cnt))
            self.clear_event(current_event_cnt)

        if self.profiling_mode:
            data.append(str(self.emg_data_buffer.timestamp))
            data.append(str(self.emg_data_buffer.seqN))

        self.log.write(self.sep_character.join(data) + "\n")
        self.emg_data_buffer = None
