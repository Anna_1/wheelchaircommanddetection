from distutils.log import warn
import numpy as np
from matplotlib import pyplot as plt
from statistics import mean
from scipy import signal
from scipy import stats
from scipy.fft import rfft, rfftfreq
from typing import Generator

from scripts import out_path, SMOOTH_SPAN, TREND_SPAN, NOTCH_FREQ, NOTCH_BANDWIDTH


def read_input_data_to_float(output_file: str) -> list:
    with open(output_file) as f:
        # работа с файлом
        data = [float(line.strip()) for line in f]  # считывание исходных данных x1(t)

    return data


def _show_or_save(show: bool, name: str = None):
    """Сохраняет график и/или выводит на экран"""
    plt.rcParams["figure.figsize"] = (20, 15)
    plt.tight_layout()
    if name:
        plt.savefig(name)
    if show:
        plt.show()
    else:
        plt.close()


def emg_fft(x_input: list, sample_time: float, name: str or None = None, show: bool = True) -> tuple:
    """Выполняет преобразование Фурье (FFT), строит графики исходного сигнала и FFT

    Keyword arguments:
    x_input -- исходный сигнал, начальное значене которого близко к нулю, например x(t)-x_mean
    sample_time -- шаг дискретизации по времени
    name -- базовое название файлов, если графики нужно сохранить, или None, если без сохранения (по умолчанию None)
    show -- надо ли отобразить графики на экране (по умолчанию False)

    """
    draw_plots = True  # нет необходимости строить графики, если пользователь не хочет их сохранять
    if not show and not name:
        draw_plots = False
        print("Warning! No plots will be shown or saved!")

    x_count = len(x_input)  # количество точек в исходном сигнале
    t = [sample_time * i for i in range(x_count)]  # время каждой точки

    if draw_plots:
        plt.plot(t, x_input)  # график сигнала x_input(t) = x(t)-x_mean
        # plt.ylim(top=15)
        # plt.ylim(bottom=-15)
        tmp = f'{name}_signal.png' if name else None
        _show_or_save(show=show, name=tmp)

    amplitude = rfft(x_input)  # вычисление амплитуд спектра. (само FFT, комплексные числа)
    f = rfftfreq(x_count, sample_time)  # вычисление частот спектра. Диапазон [0, 1/(2T)]

    amplitude_abs = list(np.abs(amplitude))  # преобразование комплексных чисел в действительные

    if draw_plots:
        plt.plot(f, amplitude_abs)  # график fft
        tmp = f'{name}_fft.png' if name else None
        _show_or_save(show=show, name=tmp)

    return list(f), list(amplitude_abs)


def n_pkf(freq: list, amplitude_abs: list, n: int) -> Generator:
    """Генерирует n пиковых частот (PKF) - частот с максимальной амплитудой, т.е. с максимальным вкладом

    Keyword arguments:
    freq -- список частот
    amplitude_abs -- список амплитуд (действительные числа) на этих частотах
    n -- сколько пиковых частот нужно отобрать из freq

    """
    amp_max = sorted(amplitude_abs, reverse=True)[:n]
    for amp in amp_max:
        i_max = amplitude_abs.index(amp)  # индекс частоты с амплитудой amp
        yield freq[i_max], amp


def rms(x_input: list) -> float:
    """Рассчитывает Root-Mean-Square (RMS)"""
    x = np.array(x_input)
    return float(np.sqrt(np.mean(x ** 2)))


def mpv(x_input: list, rms_value: float) -> float:
    """Рассчитывает Mean of Peak Values (MPV)"""
    np_data = np.array(x_input)
    mask = np_data > rms_value
    np_data = np_data[mask]
    return float(np.mean(np_data))


def mav(x_input: list) -> float:
    """Рассчитывает Mean Absolute Value (MAV)"""
    x_abs = np.abs(x_input)
    return float(np.mean(x_abs))


def wl(x_input: list) -> float:
    """Рассчитывает Wave Length (WL)"""
    np_data = np.array(x_input)
    terms = np.abs(np.diff(np_data))
    total_length = np.sum(terms)
    return float(total_length)


def zero_crossing(x_input: list) -> int:
    """Рассчитывает Zero Crossing (ZC)"""
    np_data = np.array(x_input)

    # достижение значения, равного нулю, не считается за смену знака,
    # но нулевые значения вносят искажения в дальнейшие вычисления
    no_zero_mask = np_data != 0
    np_data = np_data[no_zero_mask]

    # если значения x[i] и x[i+1] имеют разные знаки, то произошла смена знака
    # ZC(x) = SUM[i=1 to N-1](F(x[i],x[i+1])), F(a,b) = 1 if (a*b)<0 else 0
    np_data = np_data[:-1] * np_data[1:]
    zero_crossings = np.where(np_data < 0)[0]
    return len(zero_crossings)


def skw(x_input: list) -> float:
    """Рассчитывает Skewness (Ассиметрия). Характеризует ассиметрию распределения величины"""
    np_input = np.array(x_input)
    sk = stats.skew(np_input, axis=0, bias=True)
    return float(sk)


def kurt(x_input: list) -> float:
    """Рассчитывает Kurtosis (Эксцесс). Отражает, имеют ли данные
    'тяжёлые' или 'лёгкие' хвосты относительно нормального распределения"""
    k = stats.kurtosis(x_input, axis=0, bias=True)
    return float(k)


def mnf(freq: list, amplitude_abs: list) -> float:
    """Рассчитывает Mean Spectral Frequency (MNF)"""
    m = np.dot(freq, amplitude_abs)
    m = m / np.sum(amplitude_abs)
    return float(m)


def mdf(amplitude_abs: list) -> float:
    """Рассчитывает Median frequency (MDF)"""
    m = 0.5 * np.sum(amplitude_abs)
    return float(m)


def iav(x_input: list) -> float:
    """Рассчитывает Integrated Absolute Value (IAV)"""
    x_abs = np.abs(x_input)
    return float(np.sum(x_abs))


def ssc(x_input: list) -> int:
    """Рассчитывает Slope Sign Changes (SSC)"""
    # определяем знаки наклона сигнала на промежутках [X[i], X[i+1]] (знаки производной от функции сигнала)
    slope = np.diff(np.array(x_input))
    slope_sign = np.sign(slope)

    # промежутки, параллельные оси Ox, не считаются за смену знака,
    # но нулевые значения вносят искажения в дальнейшие вычисления
    no_zero_mask = slope_sign != 0
    slope_sign = slope_sign[no_zero_mask]

    # если оба значения X[i-1] и X[i+1] лежат по одну сторону от X[i], то произошла смена знака наклона
    # SSC(x) = SUM[i=2 to N-1](F(x[i]-x[i-1],x[i]-x[i+1])), F(a,b) = 1 if (a*b)>0 else 0
    sign_changes = slope_sign[:-1] * slope_sign[1:] * -1
    sign_changes = np.where(sign_changes > 0)[0]
    return len(sign_changes)


def damv(x_input: list) -> float:
    """Рассчитывает Difference Absolute Mean Value (DAMV)"""
    x_abs = np.abs(np.diff(np.array(x_input)))
    return float(np.mean(x_abs))


def dasdv(x_input: list) -> float:
    """Рассчитывает Difference absolute standard deviation value (DASDV)"""
    np_data = np.diff(np.array(x_input))
    np_data = np.sqrt(np.mean(np_data ** 2))
    return float(np_data)


def notch_filter(x_input: list, freq_to_remove: float, bandwidth: float, sample_freq: float) -> list:
    """Подавляет частоты сигнала на узкой полосе пропускания"""
    quality_factor = freq_to_remove / bandwidth

    b, a = signal.iirnotch(freq_to_remove, quality_factor, sample_freq)
    sos = signal.tf2sos(b, a)

    filtered = list(signal.sosfilt(sos, x_input))

    return filtered


def butter_filter(x_input: list, sample_freq: float) -> list:
    """Подавляет частоты сигнала при помощи фильтра Баттерворта"""
    sos = signal.butter(6, 0.7, 'highpass', fs=sample_freq, output='sos')
    filtered = list(signal.sosfilt(sos, x_input))

    return filtered


def smooth(data, span: int = 5):
    """Сглаживает сигнал скользящим окном (аналог функции SMOOTH в MATLAB)"""
    data_size = len(data)

    if span > data_size or span <= 0:
        warn("Сглаживание невозможно")
        return data

    smoothed_data = data.copy()
    span = span if span % 2 == 1 else span + 1
    hw = span // 2
    for pos in range(2, data_size + 1):
        if pos <= hw:
            i_s = 1
            i_e = 2 * pos - 1
        elif pos <= data_size - hw:
            i_s = pos - hw
            i_e = pos + hw
        else:
            i_s = pos - (data_size - pos)
            i_e = data_size

        i_s -= 1
        smooth_mean = mean(smoothed_data[i_s:i_e])
        # print(f"{smoothed_data[i_s:i_e]} {smooth_mean}")
        smoothed_data[pos - 1] = smooth_mean

    return smoothed_data


def movmean(data, span):
    """Сглаживает сигнал скользящим окном (аналог функции MOVMEAN в MATLAB)"""
    data_size = len(data)

    if span > data_size or span <= 0:
        warn("Сглаживание невозможно")
        return data

    smoothed_data = data.copy()
    left_span = span // 2
    right_span = left_span if span % 2 == 1 else left_span - 1
    for pos in range(0, data_size):
        if pos <= left_span:
            i_s = 0
            i_e = pos + right_span
        elif pos + right_span < data_size:
            i_s = pos - left_span
            i_e = pos + right_span
        else:
            i_s = pos - left_span
            i_e = data_size - 1

        i_e += 1
        smooth_mean = mean(smoothed_data[i_s:i_e])
        # print(f"{smoothed_data[i_s:i_e]} {smooth_mean}")
        smoothed_data[pos] = smooth_mean

    return smoothed_data


def gaussian_filter(data, span):
    gaussian = signal.windows.gaussian(M=span, std=2)
    gaussian /= sum(gaussian)
    convolved = np.convolve(data, gaussian, mode='valid')
    return list(convolved)


def normalize(np_data: np.array) -> np.array:
    np_min = np_data.min()
    np_max = np_data.max()
    # np_max = 600
    # normalized_data = (np_data / np_max) * 300
    normalized_data = ((np_data - np_min) / (np_max - np_min)) * 300
    return normalized_data


def signaltonoise(a, axis=0, ddof=0):
    a = np.asanyarray(a)
    m = a.mean(axis)
    sd = a.std(axis=axis, ddof=ddof)
    return np.where(sd == 0, 0, m / sd)


def emg_features(x_input: list, sample_time: float, name: str or None = None, show: bool = True) -> tuple:
    # сглаживаем форму сигнала, убирая шумы
    # x_input = movmean(data=x_input, span=SMOOTH_SPAN)
    # x_input = smooth(data=x_input, span=SMOOTH_SPAN)
    x_input = gaussian_filter(data=x_input, span=SMOOTH_SPAN)

    # удаляем постоянную составляющую сигнала (тренд)
    trend = movmean(data=x_input, span=TREND_SPAN)  # выделяем линию тренда
    x_input = [z - mm for z, mm in zip(x_input, trend)]  # вычитаем тренд из исходного сигнала

    # фильтруем сигнал
    fs = 1. / sample_time
    x_input = notch_filter(x_input=x_input, freq_to_remove=NOTCH_FREQ, bandwidth=NOTCH_BANDWIDTH, sample_freq=fs)
    x_input = butter_filter(x_input=x_input, sample_freq=fs)

    # приводим данные к одной размерности
    np_input = np.array(x_input)
    x_input = list(normalize(np_input))

    # сдвигаем нормализованный график вниз по вертикальной оси до пересечения с горизонтальной осью
    x_mean = mean(x_input)  # находим среднее значение сигнала
    x = [z - x_mean for z in x_input]  # вычитаем среднее значение из исходного сигнала. x(t) = x1-x_mean

    freq, amplitude_abs = emg_fft(x_input=x, sample_time=sample_time, name=name, show=show)
    gen = n_pkf(freq=freq, amplitude_abs=amplitude_abs, n=3)
    freq_max_dict = dict()
    i = 1
    for f_max, amp in gen:
        freq_max_dict[i] = (f_max, amp)
        i += 1

    emg_rms = rms(x_input)
    emg_mav = mav(x_input)
    emg_wl = wl(x_input)
    emg_zc = zero_crossing(x)
    emg_skew = skw(x_input)
    emg_kurt = kurt(x_input)
    emg_iav = iav(x_input)
    emg_ssc = ssc(x_input)
    emg_damv = damv(x_input)
    emg_dasdv = dasdv(x_input)
    emg_mpv = mpv(x_input, emg_rms)
    emg_mnf = mnf(freq, amplitude_abs)
    emg_mdf = mdf(amplitude_abs)
    emg_fd_wl = wl(amplitude_abs)

    return x_mean, freq_max_dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt, \
        emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl


def emg_features_generator(generator: Generator,
                           window_size: int = 256,
                           window_step: int = 128,
                           sample_time: float = 0.01,
                           name: str or None = None,
                           show: bool = True) -> Generator:
    """Генерирует весь набор фич по фреймам скользящего окна"""
    wdata = [0] * window_size  # скользящее окно
    wn = 0  # номер фрейма скользящего окна
    ln = 0  # номер считанного образца
    wfull = False  # заполнено ли окно данными
    for line in generator:
        # заполнение окна
        wdata.append(int(line))
        wdata = wdata[-window_size:]
        ln += 1

        if ln == window_size:
            wfull = True

        # сдвиг окна на window_step образцов
        if ln >= window_step and wfull:
            wname = f"{name}_{wn}" if name else None  # название графиков сигнала и БПФ по данным фрейма
            features = emg_features(x_input=wdata, sample_time=sample_time, name=wname, show=show)
            yield tuple([wn] + list(features))
            wn += 1
            ln = 0

    # чтобы не потерять последние значения
    if ln > 0:
        wname = f"{name}_{wn}" if name else None
        features = emg_features(x_input=wdata, sample_time=sample_time, name=wname, show=show)
        yield tuple([wn] + list(features))


if __name__ == '__main__':
    import os.path

    default_output_file = os.path.join(out_path, "data.txt")
    sample_t = 0.01  # Шаг дискретизации по времени. Важно ЗНАТЬ!!! Т.к. на основе этого вычисляются частоты спектра.

    print(f"output file = {default_output_file}")
    print(f"sample time = {sample_t}")

    input_data = read_input_data_to_float(default_output_file)
    emg_fft(input_data, sample_t, name="sth")
