import serial
import serial.threaded
import time

from scripts.recorder import EMGSimpleRecorder
from scripts.emg_serial_reader import EMGDataRead2Record
from scripts import port, baudrate


ser = serial.Serial(port, baudrate)

m_ch = "EVENT_MARKER"
rec = EMGSimpleRecorder(m_ch, True)
rec.sep_character = ';'

protocol = serial.threaded.ReaderThread(ser, EMGDataRead2Record)

with protocol:
    protocol.protocol.set_recorder(rec)
    protocol.protocol.recorder.start_recording()
    time.sleep(5)
    protocol.protocol.recorder.log.close()
    protocol.stop()

print("Done")
