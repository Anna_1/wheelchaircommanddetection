import fnmatch
import serial
import serial.threaded
from threading import Thread, Lock

from scripts.emg_serial_reader import EMGDataRead2Record
from scripts.recorder import Recorder
from scripts import recorder_event_codes, port, baudrate

_lock = Lock()


class RecorderManager:
    verbose: bool = False

    def __init__(self):
        self._recorder = None
        self._recorder_daemon = None

    def set_recorder(self, recorder: Recorder):
        if not self._recorder:
            self._recorder = recorder

    def start_recorder(self):
        if not self._recorder:
            return

        # child thread
        self._recorder_daemon = Thread(target=self._recorder.start_recording)
        self._recorder_daemon.daemon = True
        self._recorder_daemon.start()

    def stop_recorder(self):
        # should not stop Recorder if it wasn't started by start_recorder() method
        if not self._recorder_daemon:
            return

        self._recorder.enabled = False
        self._recorder_daemon.join()
        self._recorder.log.close()

    def update_recorder_event(self, event):
        if not self._recorder:
            return

        if self.verbose:
            print(f"Recorder set event: {event}")

        with _lock:
            self._recorder.set_event(event)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop_recorder()


class EMGRecorderManager(RecorderManager):

    def __init__(self):
        super().__init__()

        self.prev_command = "SIMPLE"

    def update_recorder_event(self, command):
        if not self._recorder:
            return

        com = self.check_command(new_command=command,
                                 previous_command=self.prev_command)

        if com != self.prev_command:
            if self.verbose:
                print(com)
            with _lock:
                self._recorder.set_event(recorder_event_codes[com])
            self.prev_command = com

    @staticmethod
    def check_command(new_command, previous_command):
        if new_command != previous_command:
            if new_command != "SIMPLE":
                return new_command
            if not fnmatch.fnmatch(previous_command, "*_UP"):
                return previous_command + "_UP"
        return previous_command


class EMGRecorderProtocolManager(EMGRecorderManager):
    def start_recorder(self):
        if not self._recorder:
            return

        # child thread
        ser = serial.Serial(port, baudrate)
        self._recorder_daemon = serial.threaded.ReaderThread(ser, EMGDataRead2Record)
        self._recorder_daemon.start()
        self._recorder_daemon.connect()
        self._recorder_daemon.protocol.set_recorder(self._recorder)
        self._recorder_daemon.protocol.recorder.start_recording()
        self._recorder = self._recorder_daemon.protocol.recorder

    def stop_recorder(self):
        # should not stop Recorder if it wasn't started by start_recorder() method
        if not self._recorder_daemon:
            return

        self._recorder_daemon.protocol.recorder.log.close()
        self._recorder_daemon.stop()
