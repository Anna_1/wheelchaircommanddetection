import keyboard
import time
import random
from threading import Thread, Lock
from typing import Generator

from scripts import command_dict

_lock = Lock()


class CommandDefinerException(Exception):
    pass


class Definer:
    get_random: bool

    _defined_command = "SIMPLE"

    def __init__(self, get_random: bool = False):
        self.get_random = get_random

    @property
    def defined_command(self):
        with _lock:
            return self._defined_command

    @defined_command.setter
    def defined_command(self, value):
        with _lock:
            self._defined_command = value


class CommandDefiner(Definer):
    # codes = [
    #     "STOP",
    #     "GO",
    #     "BACK",
    #     "RIGHT",
    #     "LEFT"
    # ]
    codes = list(command_dict.keys())[1::]

    _daemon: Thread or None = None

    def _define_commands_by_amplitude(self, feature_generator: Generator):
        for placeholder, wn, x_mean, f_max_dict, emg_rms, emg_mav, emg_wl in feature_generator:
            command = "GO"
            for key, val in f_max_dict.items():
                if val[1] > 200:
                    break
            else:
                command = "STOP"

            self.defined_command = command

    def define_commands_by_amplitude(self, feature_generator: Generator):
        if not feature_generator:
            raise CommandDefinerException("No feature generator specified!")

        try:
            self._define_commands_by_amplitude(feature_generator=feature_generator)
        except ValueError:
            raise CommandDefinerException("Incorrect feature generator specified!")

    def _random_command_daemon(self, delay: int or float = 5.0):
        while True:
            self.defined_command = random.choice(self.codes)
            time.sleep(delay)

    def get_random_command(self) -> str:
        return random.choice(self.codes)

    def start_command_daemon(self, feature_generator: Generator or None = None, delay: int or float = 5.0):
        # not recommended to start more than one daemon from one class instance
        # both use the same _defined_command value
        if self._daemon:
            return

        if self.get_random:
            self._daemon = Thread(target=self._random_command_daemon,
                                  kwargs={"delay": delay})
        else:
            self._daemon = Thread(target=self.define_commands_by_amplitude,
                                  kwargs={"feature_generator": feature_generator})
        self._daemon.daemon = True
        self._daemon.start()

    def join_command_daemon(self):
        self._daemon.join()


class KeyboardDefiner(Definer):
    _daemon: Thread or None = None

    def set_new_command(self, new_key: str = "exit"):
        self.defined_command = new_key

    def keyboard_commands(self, exit_hotkey):
        keyboard.on_press_key("q", lambda _: self.set_new_command("WHEELCHAIR"))
        keyboard.on_press_key("space", lambda _: self.set_new_command("STOP"))
        keyboard.on_press_key("w", lambda _: self.set_new_command("GO"))
        keyboard.on_press_key("s", lambda _: self.set_new_command("BACK"))
        keyboard.on_press_key("d", lambda _: self.set_new_command("RIGHT"))
        keyboard.on_press_key("a", lambda _: self.set_new_command("LEFT"))
        keyboard.on_release(lambda _: self.set_new_command("SIMPLE"))

        keyboard.add_hotkey(exit_hotkey, lambda: self.set_new_command())

    def start_command_daemon(self, exit_hotkey="Ctrl + C"):
        # not recommended to start more than one daemon from one class instance
        if self._daemon:
            return

        self._daemon = Thread(target=self.keyboard_commands,
                              kwargs={"exit_hotkey": exit_hotkey})
        self._daemon.daemon = True
        self._daemon.start()

    def join_command_daemon(self):
        self._daemon.join()


if __name__ == '__main__':
    from scripts.realtime_scan import realtime_feature_generator_wrapper as _feature_generator

    loop_delay = 3

    gen = _feature_generator(save_plots=False)
    cd = CommandDefiner()
    cd.start_command_daemon(feature_generator=gen)

    cd_r = CommandDefiner(get_random=True)
    cd_r.start_command_daemon(delay=5)

    while True:
        print("Cont: " + cd.defined_command)
        print("Cont rand: " + cd_r.defined_command)
        time.sleep(loop_delay)
