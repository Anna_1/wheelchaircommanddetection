from mock import patch, Mock
import random

from scripts import emg_gui
import scripts.emg_serial_reader


def readline(*args, **kwargs):
    line = str(random.randint(280, 320)).encode("utf-8")
    return line


def read(*args, **kwargs):
    line = str(random.randint(280, 320)).encode("utf-8") + b'\xa5'
    return line


def fake_serial():
    ser = Mock()
    ser.is_open = True
    ser.read = read
    ser.readline = readline
    ser.in_waiting = len(ser.read())

    return ser


def handle_packet(self, packet):
    # print("im here in mock handle")
    if self.skip > 0:  # пропускаем первые skip сэмплов
        self.skip -= 1
        return
    # print("skipped")
    emg_ds = Mock()
    emg_ds.seqN = random.randint(100, 200)
    emg_ds.timestamp = random.randint(1000, 2000)
    emg_ds.signal = random.randint(280, 320)
    emg_ds.disconnects = 0
    emg_ds.package_lost = 0
    self.emg_data_buffer = emg_ds
    # print(f"mock handle  {self.emg_data_buffer.signal}")


def test_fake_serial():
    ser = fake_serial()

    for i in range(10):
        sig = ser.read()
        print(f"{i} {sig}")


@patch("serial.Serial")
@patch.object(scripts.emg_serial_reader.EMGDataReader, 'handle_packet', handle_packet, spec=scripts.emg_serial_reader.EMGDataReader)
def test_emg_gui(serial_mock):
    serial_mock.return_value = fake_serial()

    emg_gui.run(iter_count=2, profiling_mode=True)


# test_fake_serial()
test_emg_gui()


# туторы по работе с моками
# https://digitology.tech/docs/python_3/library/unittest.mock-examples.html
# https://ru.hexlet.io/courses/python-advanced-testing/lessons/mocking/theory_unit
# https://habr.com/ru/articles/134836/
