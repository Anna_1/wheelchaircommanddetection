from typing import Generator
import os.path

from scripts import out_path, command_dict, name_type, result_path
from scripts import functions
import scripts.EMG_fft as Ef


def readfile_generator(input_file: str) -> Generator:
    """Read input data x(t) from file"""
    with open(input_file) as f:
        for line in f:
            yield line.strip()


def append_result_line(command: str, file_name: str, frame: int,
                       x_mean, f_max_dict: dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt,
                       emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl,
                       res_file_name: str,
                       text: bool = True,
                       csv: bool = True):
    """Print results and append them to .txt or .csv file in appropriate format"""
    result_string = ("%s %s_%d mean=%.3f RMS=%.3f MAV=%.3f WL=%.3f ZC=%d SKEW=%.3f KURT=%.3f "
                     "IAV=%.3f SSC=%d DAMV=%.3f DASDV=%.3f MPV=%.3f MNF=%.3f MDF=%.3f FD_WL=%.3f"
                     % (command, file_name, frame, x_mean, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt,
                        emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl))
    for key, val in f_max_dict.items():
        result_string += (" f_max%d=%.3f amp%d=%.3f" % (key, val[0], key, val[1]))

    print(result_string)

    if text:
        with open(f"{res_file_name}.txt", 'a', encoding='utf-8') as f:
            f.write(f"{result_string}\n")
    if csv:
        result_string = ("%s;%s;%d;%.3f;%.3f;%.3f;%.3f;%d;%.3f;%.3f;%.3f;%d;%.3f;%.3f;%.3f;%.3f;%.3f;%.3f"
                         % (command, file_name, frame, x_mean, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt,
                            emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl))
        for key, val in f_max_dict.items():
            result_string += (";%.3f;%.3f" % (val[0], val[1]))
        with open(f"{res_file_name}.csv", 'a', encoding='utf-8') as f:
            f.write(f"{result_string}\n")


def process_data_file(command: str,
                      window_size: int, window_step: int, sample_time: float,
                      input_file: str, output_plot_name: str or None, res_file_name: str,
                      file_name: str = "-",
                      text_result: bool = True,
                      csv_result: bool = True):
    """Fully process single data file"""
    print(input_file)
    print(f"sample time = {sample_time}")

    gen = readfile_generator(input_file)

    efg = Ef.emg_features_generator(generator=gen,
                                    window_size=window_size,
                                    window_step=window_step,
                                    sample_time=sample_time,
                                    name=output_plot_name,
                                    show=False)

    for wn, x_mean, f_max_dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt, \
            emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl in efg:
        append_result_line(command, file_name, wn,
                           x_mean, f_max_dict, emg_rms, emg_mav, emg_wl, emg_zc, emg_skew, emg_kurt,
                           emg_iav, emg_ssc, emg_damv, emg_dasdv, emg_mpv, emg_mnf, emg_mdf, emg_fd_wl,
                           res_file_name, text_result, csv_result)


def process_data_files_by_command(command: str, input_command_path: str,
                                  window_size: int, window_step: int,
                                  sample_time: float,
                                  res_command_path: str,
                                  res_file_name: str,
                                  text_result: bool = True,
                                  csv_result: bool = True):
    """Process all data files related to a specific command

    Keyword arguments:
    command -- the command name that will be used as a key for saving results (plots and features)
    input_command_path -- directory with data files that should be processed
    window_size -- running window size
    window_step -- running window increment step
    sample_time -- sample time for FFT calculation
    res_command_path -- existing directory to save results for specified command
    res_file_name -- base name for result files, can be set as "" if both text_result and csv_result are False
    text_result -- defines if results should be appended to .txt file (default True)
    csv_result -- defines if results should be appended to .csv file (default True)

    """
    for filename in os.listdir(input_command_path):
        input_file = os.path.join(input_command_path, filename)
        short_name = str(filename.split('.')[0])
        output_plot_name = os.path.join(res_command_path, short_name)

        process_data_file(command=command, file_name=short_name,
                          window_size=window_size, window_step=window_step, sample_time=sample_time,
                          input_file=input_file, output_plot_name=output_plot_name,
                          res_file_name=res_file_name, text_result=text_result, csv_result=csv_result)

        print("==============")


def default_process():
    """Define default workflow and variables for this module"""
    window_size = 2048
    window_step = 512
    # sample_time = 0.000868  # ~1152 Hz
    sample_time = 0.001  # 1000 micros (1000 Hz)

    if name_type == 2:
        window_size = 64
        window_step = 32
        sample_time = 0.1

    clean_previous_results = False
    text_result = True
    csv_result = True

    functions.prepare_directory(result_path, clean_previous_results)

    for key, val in command_dict.items():
        print(f"Следующая команда: {val}")

        inp_command_path = os.path.join(out_path, key)
        res_command_path = os.path.join(result_path, key)
        functions.prepare_directory(res_command_path)
        res_file = os.path.join(res_command_path, "res")

        if text_result and csv_result:
            functions.prepare_file(f"{res_file}.txt")
            functions.prepare_file(f"{res_file}.csv")

            with open(f"{res_file}.csv", 'a', encoding='utf-8') as rf_csv:
                rf_csv.write("name;fileN;frame;mean;RMS;MAV;WL;ZC;SKEW;KURT;"
                             "IAV;SSC;DAMV;DASDV;MPV;MNF;MDF;FD_WL;f_max1;amp1;f_max2;amp2;f_max3;amp3\n")
        else:
            res_file = ""

        process_data_files_by_command(command=key, input_command_path=inp_command_path,
                                      window_size=window_size, window_step=window_step,
                                      sample_time=sample_time, res_command_path=res_command_path,
                                      res_file_name=res_file, text_result=text_result, csv_result=csv_result)


if __name__ == '__main__':
    default_process()
