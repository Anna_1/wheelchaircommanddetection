import serial
import time

from scripts.recorder import EMGSimpleRecorder
from scripts.recorder_manager import EMGRecorderProtocolManager


m_ch = "EVENT_MARKER"
rec = EMGSimpleRecorder(m_ch, True)
rec.sep_character = ';'

rec_manager = EMGRecorderProtocolManager()
rec_manager.set_recorder(rec)

rec_manager.start_recorder()
time.sleep(10)

test_command = "BACK"
rec_manager.update_recorder_event(command=test_command)
time.sleep(5)

test_command = "SIMPLE"
rec_manager.update_recorder_event(command=test_command)
time.sleep(5)

rec_manager.stop_recorder()
