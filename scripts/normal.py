import os.path
import scipy.stats
import pandas as pd
import fnmatch
from matplotlib import pyplot as plt

from scripts import out_path, command_dict
from scripts import functions


class BoxSubplots:
    _plots = dict()

    x = 0
    y = 0

    def __init__(self, max_boxplot_x: int, max_boxplot_y: int):
        self.max_x = max_boxplot_x
        self.max_y = max_boxplot_y

    def append(self, plot_suptitle):
        fig, axs = plt.subplots(self.max_x, self.max_y)
        fig.suptitle(plot_suptitle)
        self._plots[plot_suptitle] = (fig, axs)

    def update(self, plot_suptitle, new_data, title):
        if plot_suptitle not in self._plots:
            self.append(plot_suptitle=plot_suptitle)

        ax = self._plots[plot_suptitle][1]
        ax[self.x, self.y].boxplot(new_data)
        ax[self.x, self.y].set_title(title)

    def inc(self):
        self.y += 1
        if self.y >= self.max_y:
            self.x += 1
            self.y = 0

    def save(self, plot_suptitle, name):
        fig = self._plots[plot_suptitle][0]
        fig.set_size_inches(20, 15)
        fig.tight_layout()
        fig.savefig(name)

    def save_all(self, base_result_path):
        for plot_suptitle in self._plots:
            box_path = os.path.join(base_result_path, f"total_boxplot_{plot_suptitle}.png")
            self.save(plot_suptitle=plot_suptitle, name=box_path)

    def show_all(self):
        for plot_suptitle in self._plots:
            fig = self._plots[plot_suptitle][0]
            fig.tight_layout()
        plt.show()

    def show_or_save_all(self, show: bool, base_result_path: str = None):
        if base_result_path:
            self.save_all(base_result_path=base_result_path)
        if show:
            self.show_all()
        else:
            plt.close()


def _show_or_save(show: bool, name: str = None):
    """Сохраняет график и/или выводит на экран"""
    if name:
        plt.savefig(name)
    if show:
        plt.show()
    else:
        plt.close()


def mk_plot(input_data: list,
            command_name: str,
            feature_name: str,
            plot_name: str or None = None,
            show: bool = False):
    if not show and not plot_name:
        print("Warning! No plots will be shown or saved!")

    # Draw the plot
    plt.hist(input_data, bins=int(180 / 5),
             color='blue', edgecolor='black')

    plt.title(f"{command_name} {feature_name}")
    plt.tight_layout()
    _show_or_save(show=show, name=plot_name)


def mk_boxplot(input_data: list,
               feature_labels: list,
               command_name: str,
               feature_name: str = "",
               plot_name: str or None = None,
               show: bool = False):
    if not show and not plot_name:
        print("Warning! No plots will be shown or saved!")

    # Draw the plot
    plt.boxplot(input_data, labels=feature_labels)

    plt.title(f"{command_name} {feature_name}")
    plt.tight_layout()
    _show_or_save(show=show, name=plot_name)


def prepare_data(file_path: str, sep: str = ";"):
    data = pd.read_csv(file_path, sep=sep)
    data.head()

    feature_list = [f for f in data if f not in ['name', 'fileN', 'frame'] and not fnmatch.fnmatch(f, "amp*")]
    return data[feature_list]


def shapiro_test(input_data, alpha, feature_name=""):
    stat, p = scipy.stats.shapiro(input_data)
    if p > alpha:
        print('Шапиро-Уилк %s statistics=%.3f, p-value=%.3f' % (feature_name, stat, p))


def pirson_test(input_data, alpha, feature_name=""):
    stat, p = scipy.stats.normaltest(input_data)
    if p > alpha:
        print('Пирсон %s statistics=%.3f, p-value=%.3f' % (feature_name, stat, p))


def normal_tests(alpha, input_path: str, input_file: str, sep: str = ";"):
    print(f"Критическое значение = {alpha}")
    print('============')

    for command in command_dict:
        print(command)
        inp = os.path.join(input_path, command, input_file)
        data = prepare_data(file_path=inp, sep=sep)

        for feature in data:
            # тест Шапиро-Уилк
            shapiro_test(input_data=data[feature], alpha=alpha, feature_name=feature)

            # Критерий согласия Пирсона
            pirson_test(input_data=data[feature], alpha=alpha, feature_name=feature)

        print('============')


def mk_simple_plots(input_path: str,
                    input_file: str,
                    sep: str = ";",
                    base_result_path: str or None = None,
                    show: bool = False):
    print("Диаграммы")
    for command in command_dict:
        command_path = os.path.join(base_result_path, command) if base_result_path else None
        functions.prepare_directory(command_path)

        print(command)
        inp = os.path.join(input_path, command, input_file)
        data = prepare_data(file_path=inp, sep=sep)

        for feature in data:
            # гистограмма
            path = os.path.join(command_path, f"{feature}.png") if command_path else None
            mk_plot(input_data=data[feature], command_name=command,
                    feature_name=feature, plot_name=path, show=show)

            # ящик с усами
            box_path = os.path.join(command_path, f"{feature}_boxplot.png") if command_path else None
            mk_boxplot(input_data=data[feature], feature_labels=[feature], command_name=command,
                       feature_name=feature, plot_name=box_path, show=show)

        # ящики с усами по всем фичам на одном графике
        box_path = os.path.join(command_path, "total_command_boxplot.png")
        mk_boxplot(input_data=data, feature_labels=data.keys(), command_name=command,
                   plot_name=box_path, show=show)

        print('============')


def mk_total_feature_boxplots(max_boxplot_y: int,
                              input_path: str,
                              input_file: str,
                              sep: str = ";",
                              base_result_path: str or None = None,
                              show: bool = False):
    print("Ящики с усами, сгруппированные по фичам")
    max_boxplot_x = len(command_dict.keys()) // max_boxplot_y + 1
    bplt = BoxSubplots(max_boxplot_x=max_boxplot_x, max_boxplot_y=max_boxplot_y)

    for command in command_dict:
        print(command)
        inp = os.path.join(input_path, command, input_file)
        data = prepare_data(file_path=inp, sep=sep)

        for feature in data:
            bplt.update(plot_suptitle=f"{feature}", new_data=data[feature], title=command)

        bplt.inc()
        print('============')

    bplt.show_or_save_all(show=show, base_result_path=base_result_path)


if __name__ == '__main__':
    separator = ';'
    res_dir = os.path.join(out_path, "res_libtests")
    res_file = "res.csv"

    norm_dir = os.path.join(out_path, "norm_libtests")
    functions.prepare_directory(norm_dir, clean=True)

    Alpha = 0.05
    max_boxplot_columns = 3

    # Тесты нормального распределения
    normal_tests(alpha=Alpha, input_path=res_dir, input_file=res_file, sep=separator)

    # Гистограммы и ящики с усами, сгруппированные по командам
    mk_simple_plots(input_path=res_dir, input_file=res_file, sep=separator,
                    base_result_path=norm_dir, show=False)

    # Ящики с усами, сгруппированные по фичам. Отдельная функция, чтоб графики не мешали друг другу
    mk_total_feature_boxplots(max_boxplot_y=max_boxplot_columns,
                              input_path=res_dir, input_file=res_file, sep=separator,
                              base_result_path=norm_dir, show=False)
