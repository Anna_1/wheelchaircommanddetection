import os.path
import time

from scripts import emg_gui
from scripts import functions
from scripts import out_path, recorder_event_codes
from scripts.command_definer import KeyboardDefiner
from scripts.recorder import EMGRecorder
from scripts.recorder_manager import EMGRecorderManager

sep_character = ';'
iter_count = 2

codes_reversed = {val: key for key, val in recorder_event_codes.items()}


def run_with_keyboard():
    # child thread 1 (keyboard)
    definer = KeyboardDefiner()
    definer.start_command_daemon(exit_hotkey="Ctrl + C")
    print("Waiting for 5 sec...")
    time.sleep(5)
    print("Ready")

    # child thread 2 (recorder)
    m_ch = "EVENT_MARKER"
    rec = EMGRecorder(m_ch)
    rec.sep_character = sep_character
    rec_manager = EMGRecorderManager()
    rec_manager.verbose = True
    rec_manager.set_recorder(recorder=rec)
    rec_manager.start_recorder()

    # main thread
    while True:
        com = definer.defined_command
        if com == "exit":
            break

        rec_manager.update_recorder_event(command=com)

    definer.join_command_daemon()
    rec_manager.stop_recorder()


def run_with_gui():
    emg_gui.run(iter_count=iter_count, sep_character=sep_character)


def shared_log_to_samples(output_path: str = out_path,
                          log_file: str = "log.csv",
                          clean_previous_data: bool = False,
                          first_file_id: int = 0):
    functions.prepare_directory(output_path, clean_previous_data)
    files_nums = dict()
    output_file = ""
    record = False

    with open(log_file, 'r') as inp_f:
        inp_f.readline()  # skip header
        for line in inp_f:
            current_vals = line.strip().split(sep_character)
            event_code = int(current_vals[1])
            if event_code != 0:  # skips SIMPLE samples
                print(current_vals, codes_reversed[event_code])
                if event_code % 10 != 0:
                    record = False
                else:
                    record = True
                    command = codes_reversed[event_code]
                    functions.prepare_directory(os.path.join(output_path, command))
                    num = files_nums.setdefault(command, first_file_id)
                    output_file = os.path.join(output_path, command, f"{num}.txt")
                    functions.prepare_file(output_file)
                    files_nums[command] += 1

            if record:
                with open(output_file, 'a') as o_f:
                    o_f.write(f"{current_vals[0]}\n")


if __name__ == '__main__':
    # run_with_keyboard()
    run_with_gui()
    # shared_log_to_samples(output_path="output-new", log_file="log_2000ms_2times_normal.csv",
    #                       clean_previous_data=False, first_file_id=1)
