import datetime
import os.path
import serial

from scripts import port, baudrate, out_path, command_dict
from scripts import functions


def simple_run_in_range(serial_port: serial.Serial, output_file: str, read_count: int):
    for i in range(read_count):
        emg_signal = functions.read_emg_once(serial_port)
        if emg_signal:
            with open(output_file, 'a') as f:  # do not move upper cas if fails nothing would be written to file
                f.write(f"{emg_signal}\n")


def run(serial_port: serial.Serial, ru_command: str, output_file: str, read_count: int):
    functions.prepare_file(output_file)

    print("Следующая команда:")
    print(ru_command)
    input("Нажмите Enter для продолжения...")

    t_start = datetime.datetime.now()
    simple_run_in_range(serial_port, output_file, read_count)
    t_stop = datetime.datetime.now()

    delta = (t_stop - t_start).total_seconds()      # общее время выполнения (в секундах)
    average_sample_t = delta/read_count             # шаг дискретизации по времени (в секундах)
    average_sample_t = round(average_sample_t, 3)   # округление до трёх знаков после запятой
    print(str(average_sample_t))
    with open(output_file, 'a') as f:
        f.write(f"{average_sample_t}\n")


if __name__ == '__main__':
    # Connect to serial port
    # Serial writes data each 10ms, so sample time (ST) is 0.01 by default
    # According to tests, real ST is about 0.0100119s, that can be rounded to 0.01
    # If script is paused, port continues writing data, so calculated ST can be lower
    ser = serial.Serial(port, baudrate)

    clean_previous_data = False
    r_count = 1024
    N = 10

    functions.prepare_directory(out_path, clean_previous_data)
    for i in range(0, N):
        for key, val in command_dict.items():
            functions.prepare_directory(os.path.join(out_path, key))
            of = os.path.join(out_path, key, f"{i}.txt")
            print(of)
            run(serial_port=ser, ru_command=val, output_file=of, read_count=r_count)
